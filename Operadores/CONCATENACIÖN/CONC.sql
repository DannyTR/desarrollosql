SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;
SELECT employee_id, LPAD(last_name, LENGTH(last_name) + (LEVEL*2)-2, '_') AS
ORG_CHART
FROM employees
WHERE last_name != 'Zlotkey'
START WITH last_name = 'Grant'
CONNECT BY employee_id = PRIOR manager_id;